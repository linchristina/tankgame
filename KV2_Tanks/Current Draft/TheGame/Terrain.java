package TheGame;

import java.io.Serializable;

public class Terrain implements Serializable{

	private int x;
	private int y;

	public Terrain(int x, int y){
		this.x=x;
		this.y=y;
	}
	public int getX(){return x;}
	public int getY(){return y;}
}
