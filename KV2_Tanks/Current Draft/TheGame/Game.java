package TheGame;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

public class Game implements Serializable{
	private ArrayList<Terrain> field;
	private int numPlayers;
	private TurnController turn;
	private int Bw;
	private int Bh;
	private ArrayList<Tank> tanks;
	private ArrayList<String> names;
	private String specialCase;
	private String sound;
	
	//Statistics on each player after game ends
	private ArrayList<String> databaseInfo =null;
	
	public Game(int Bw, int Bh, int numPlayers){
		this.Bw=Bw;
		this.Bh=Bh;
		field = new ArrayList<Terrain>();
		this.numPlayers=numPlayers;
		turn = new TurnController(this, numPlayers);
		tanks = new ArrayList<Tank>();
		specialCase = null;
		names = new ArrayList<String>();
		sound=null;
	}
	
	public void setField(ArrayList<Terrain> newField){ field = newField; }
	public ArrayList<Terrain> getField(){ return field; }
	public void setTurn(int newTurn){ turn.setTurn(newTurn); }
	public int getTurn(){ return turn.getTurn(); }
	public void setSpecialCase(String newSpecialCase){ specialCase = newSpecialCase; }
	public String getSpecialCase(){ return specialCase; }
	public void addTank(Tank t){ tanks.add(t); }
	public int getCurrentTank(){ return turn.getCurrentTank(); }
	public void setCurrentTank(int c){ turn.setCurrentTank(c); }
	public ArrayList<Tank> getTanks(){ return tanks; }
	public void setTanks(ArrayList<Tank> tanks){ this.tanks = tanks; }
	public boolean hasSpecialCase(){ return (specialCase!=null); }
	
	public void addName(String s){	names.add(s); }
	public ArrayList<String> getNames(){ return names; }
	public void setNames(ArrayList<String> n){ names = n; }

	public ArrayList<String> getDatabaseInfo(){ return databaseInfo; }
	public void setDatabaseInfo(ArrayList<String> info){ databaseInfo=info; }
	
	public String getSound(){ return sound; }
	public void setSound(String s){ sound = s; }
	
	public void nextTurn(){ 
		turn.next();
	}
	
	public int getBw(){return Bw;}
	public int getBh(){return Bh;}
	public int getNumPlayers(){return numPlayers;}
	
	public Game copyOf(){
		Game retval = new Game(Bw, Bh, numPlayers);
		retval.setCurrentTank(turn.getCurrentTank());
		retval.setField(copyList(getField()));
		retval.setTurn(getTurn());
		retval.setSpecialCase(getSpecialCase());
		retval.setTanks(copyTanks(tanks, retval));
		retval.setNames(copyNames(names));
		if(databaseInfo!=null)retval.setDatabaseInfo(copyNames(databaseInfo));
		retval.setSound(getSound());
		return retval;
	}
	
	public String toString(){
		String ret = Bw+","+Bh+","+numPlayers;
		ret = ret+","+turn.getCurrentTank();
		ret = ret+","+turn.getTurn();
		ret = ret+","+fieldToString();
		ret = ret+","+tanksToString();
		ret = ret+","+namesToString();
		return ret;
	}
	public String fieldToString(){
		String ret="";
		for(Terrain t : field){
			if(t instanceof Hill){
				ret+="0";
			}else if(t instanceof Pit){
				ret+="1";
			}else if(t instanceof Obstacle){
				ret+="2";
			}else{
				ret+="3";				
			}
		}
		return ret;
	}
	public static ArrayList<Terrain> fieldFromString(String f, int Bw){
		ArrayList<Terrain> ret= new ArrayList<Terrain>();
		for(int i=0;i<f.length();i++){
			if(f.charAt(i)=='0'){
				ret.add(new Hill(i%Bw,i/Bw));
			}else if(f.charAt(i)=='1'){
				ret.add(new Pit(i%Bw,i/Bw));
			}else if(f.charAt(i)=='2'){
				ret.add(new Obstacle(i%Bw,i/Bw));				
			}else{
				ret.add(new Terrain(i%Bw,i/Bw));
			}
		}
		return ret;
	}
	public String tanksToString(){
		String ret="";
		for(Tank t : tanks){
			ret=ret+t.getX();
			ret=ret+"-"+t.getY();
			ret=ret+"-"+t.getDir();
			ret=ret+"-"+t.getHP();
			ret=ret+"-"+t.getMP();
			ret=ret+"-"+t.getId();
			ret=ret+"-"+t.getMPthisTurn();
			ret=ret+"~";
		}
		return ret.substring(0, ret.length()-1);
	}
	public static ArrayList<Tank> tanksFromString(String s, Game newGame){
		ArrayList<Tank> ret = new ArrayList<Tank>();
		String [] ts = s.split("~");
		for(String t : ts){
			String []parse = t.split("-");
			Tank cur = new Tank(Integer.parseInt(parse[0]),
								Integer.parseInt(parse[1]),
								Integer.parseInt(parse[2]),
								Integer.parseInt(parse[3]),
								Integer.parseInt(parse[4]),
								Integer.parseInt(parse[5]), newGame);
			cur.setMPthisTurn(Integer.parseInt(parse[6]));
			ret.add(cur);
		}
		return ret;
	}
	public String namesToString(){
		String ret="";
		for(String s : names){
			ret=ret+s+"-";
		}
		return ret.substring(0, ret.length()-1);
	}
	public static ArrayList<String> namesFromString(String s){
		ArrayList<String> ret=new ArrayList<String>();
		for(String n : s.split("-")){
			ret.add(n);
		}
		return ret;
	}
	public static Game fromString(String g){
		String[] parse = g.split(",");
		Game retval = new Game(Integer.parseInt(parse[0]), Integer.parseInt(parse[1]), Integer.parseInt(parse[2]));
		retval.setCurrentTank(Integer.parseInt(parse[3]));
		retval.setTurn(Integer.parseInt(parse[4]));
		retval.setField(fieldFromString(parse[5], Integer.parseInt(parse[0])));
		retval.setTanks(tanksFromString(parse[6], retval));
		retval.setNames(namesFromString(parse[7]));
		System.out.println(retval.toString());
		return retval;
	}
	
	public ArrayList<Terrain> copyList(ArrayList<Terrain> list){
		ArrayList<Terrain> retval = new ArrayList<Terrain>();
		for(Terrain i : list){
			retval.add(i);
		}
		return retval;
	}
	public ArrayList<String> copyNames(ArrayList<String> list){
		ArrayList<String> retval = new ArrayList<String>();
		for(String i : list){
			retval.add(i);
		}
		return retval;
	}
	public ArrayList<Tank> copyTanks(ArrayList<Tank> list, Game ret){
		ArrayList<Tank> retval = new ArrayList<Tank>();
		for(Tank i : list){
			Tank n = new Tank(i.getX(), i.getY(), i.getDir(), i.getHP(), i.getMP(), i.getId(), ret);
			n.setObserving(i.getObserving());
			n.setScanning(i.getScanning());
			n.setMPthisTurn(i.getMPthisTurn());
			n.setDirectedScanPoint(i.getDirectedScanPoint());
			retval.add(n);
		}
		return retval;
	}
	public boolean hasTank(int x, int y){
		for(Tank t : tanks){
			if(t.isDead())continue;
			if(t.getX()==x && t.getY()==y ) return true;
		}
		return false;
	}
	public Tank getTank(int x, int y){
		for(Tank t : tanks){
			if(t.isDead())continue;
			if(t.getX()==x && t.getY()==y) return t;
		}
		return null;
	}
	public int countLivingTanks(){
		int r=0;
		for(Tank t : tanks){
			if(!t.isDead()) r++;
		}
		return r;
	}
	public void setStatistics(UpdateInfo db){
		databaseInfo = new ArrayList<String>();
		for(String s : names){
			try {
				databaseInfo.add(db.getUserInfo(s));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public int winner(){
		int max_loc=-1;
		int max_mp=-1;
		boolean tie=false;
		int r=0;
		for(Tank t : tanks){
			if(!t.isDead() && t.getMP()==max_mp) tie=true;
			if(!t.isDead() && t.getMP()>max_mp){
				max_loc=r;
				max_mp = t.getMP();
				tie=false;
			}
			r++;
		}
		if(tie) return -1;
		else return max_loc;
	}
}
