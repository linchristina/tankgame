package TheGame;

import java.awt.*;
import Buttons.*;
import java.awt.event.*; 
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

import java.net.URL;
import javax.sound.sampled.*;

public class TankScreen implements ActionListener {
	/* This object will show the graphics of the game and interface with the user.
	 * It will be given updates to the board from its corresponding TankClient, which will
	 * 	call public methods and variables on this object to change it:
	 * 			-update( GameInfo info )
	 * 			-becomeBoard()
	 * 			-createOptionScreen()
	 * 			-close()
	 * 			-Bw, Bh, numOfPlayers 
	 * This will contain the main method of the program.
	 */
	
	public int Bw;
	public int Bh;
	public int numOfPlayers;
	public int turnsPerPlayer;
	public int timePerTurn;
	public int tankHealth;
	
	public int playerNum;
	private boolean myTurn=false;
	private ArrayList<JButton> buttonsList;
	private Player client;
	
	private JFrame optionFrame;
	private JFrame boardFrame;
	private JTextField boardWidthText;
	private JTextField boardHieghtText;
	private JTextField numOfPlayersText;
	private JTextField turnsPerPlayerText;
	private JTextField timePerTurnText;
	private JTextField tankHealthText;
	private ArrayList<JLabel> gameLabels;
	
	private Timer clockUpdate;
	
	private TankGraphics icons;
	public void setNames(ArrayList<String> n){icons.setNames(n);}
	
	private String name = "";
	public String getName(){ return name; }
	public void setName(String n){ name = n; }
	
	public Game mostRecentInfo;
	
	public TankScreen(){
		buttonsList = new ArrayList<JButton>();
		gameLabels = new ArrayList<JLabel>();
		
		client = new Player(this);
		Thread t = new Thread(client);
		t.start();
		
		icons = new TankGraphics();
		
		optionFrame = new JFrame();
		optionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		optionFrame.setLayout(new BorderLayout());
		optionFrame.setSize(new Dimension(600, 500));
		optionFrame.setTitle("Tanks");
		JLabel background = new JLabel();
		background.setIcon(icons.getBackgroundPic());
		
		NextButton creator = new NextButton("Create a game", client);
		creator.addActionListener(this);
		LoadButton load = new LoadButton("Load a game", client);
		load.addActionListener(this);
		JoinButton joiner = new JoinButton("Join a game", client);
		joiner.addActionListener(this);
		JPanel options = new JPanel(new GridLayout(3,1));
		options.add(creator);
		options.add(load);
		options.add(joiner);
		
		optionFrame.add(background);
		optionFrame.add(options, BorderLayout.SOUTH);
		optionFrame.setResizable(false);
		optionFrame.setVisible(true);
	}

	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		boolean goodInfo = true;
		if(source instanceof CreateButton){
			try {
				Bw = Integer.parseInt(boardWidthText.getText());
				Bh = Integer.parseInt(boardHieghtText.getText());
				numOfPlayers = Integer.parseInt(numOfPlayersText.getText());
				turnsPerPlayer = Integer.parseInt(turnsPerPlayerText.getText());
				timePerTurn = Integer.parseInt(timePerTurnText.getText());
				tankHealth = Integer.parseInt(tankHealthText.getText());
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Invalid Information");
				goodInfo = false;
			}
		}
		if(goodInfo){
			TankButton button = (TankButton) source; 
			button.act();
		}
	}
	
	public void update(Game info){
		//This method is called by the client thread. It will update the screen to show the new position
		//of pieces on the board and the new scores. All this information can be found in the GoInfo
		//passed in through the parameter
		mostRecentInfo=info;
		
		if(info.getSound()!=null){
			soundEffect(info.getSound()+".wav");
		}
		if(!myTurn && info.getCurrentTank()==playerNum){
			myTurn=true;
			gameLabels.get(1).setText(""+timePerTurn);
			int delay = 1000; //milliseconds
			  ActionListener taskPerformer = new ActionListener() {
			      public void actionPerformed(ActionEvent evt) {
			    	  if(Integer.parseInt(gameLabels.get(1).getText())==0){
			    		  gameLabels.get(0).setText("");
			    		  client.pass();
			    		  clockUpdate.stop();
			    	  }
			    	  gameLabels.get(1).setText(""+(Integer.parseInt(gameLabels.get(1).getText())-1));
			      }
			  };
			  clockUpdate = new Timer(delay, taskPerformer);
			  clockUpdate.start();
		}
		if(myTurn && info.getCurrentTank()!=playerNum){
			myTurn=false;
			gameLabels.get(1).setText("");
			clockUpdate.stop();
		}
		
		if(info.hasSpecialCase()){
			evaluate(info);
		}else{		
			updateFrame(info);
		}
	}
	
	private void evaluate(Game info){
		//this method is called when updating the screen but the info had a special case. This method
		//will handle the case of GameOver, Invalid Moves, and moving when it is not your turn. 
		//The latter two will be resolved simply by showing a pop up
		String specialCase = info.getSpecialCase();
		if(specialCase.equals("Game Over")){
			boardFrame.setVisible(false);
			TankScreen game = new TankScreen();
			createScoreBoard(info);
		}
		if(specialCase.equals("Invalid Move")){
			JOptionPane.showMessageDialog(null, "Invalid move");
		}
		if(specialCase.equals("Not Your Turn")){
			JOptionPane.showMessageDialog(null, "It is not your turn.");
		}
		if(specialCase.equals("Wait")){
			JOptionPane.showMessageDialog(null, "Waiting for more players.");
		}
		if(specialCase.substring(0,3).equals("hit")){
			int source = parseHitSource(specialCase);
			int target = parseHitTarget(specialCase);
			if(playerNum==source){
				JOptionPane.showMessageDialog(null, "You hit someone!");
			}
			if(playerNum==target){
				JOptionPane.showMessageDialog(null, "You've been hit!");
			}
			updateFrame(info);
		}
	}

	public void becomeBoard() 
	throws IOException{
	//this method is called by the client once it has connected to a game, received the board size, and
	//set this values for the game into the public variables of this object.
		JPanel gameBoard = createGameBoard();
		JPanel sideInfo = createSideInfo();
				
		optionFrame.setVisible(false);
		boardFrame = new JFrame();
		boardFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		boardFrame.setLayout(new BorderLayout());
		boardFrame.setSize(new Dimension(33*Bw + 220, 33*Bh+40));
		boardFrame.setResizable(false);
		boardFrame.setTitle("Tanks");
		boardFrame.add(gameBoard);
		boardFrame.add(sideInfo, BorderLayout.EAST);
		boardFrame.setVisible(true);
	}
	
	private void createScoreBoard(Game info) {
		//This method creates a new frame that pops up and displays the name of the winner and the score
		JFrame scoreCard = new JFrame();
		scoreCard.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		scoreCard.setLayout(new GridLayout(numOfPlayers+1, 3));
		scoreCard.setSize(new Dimension(250, 250));
		scoreCard.setResizable(false);
		scoreCard.setVisible(true);
		
		int winner = info.winner();
		scoreCard.add(new JLabel("Winner:"));
		if(winner==-1){
			scoreCard.add(new JLabel("Its a Tie"));
		}else{
			scoreCard.add(new JLabel(icons.playerName(info.winner())));
		}
		scoreCard.add(new JLabel(""));
		for(String s : info.getDatabaseInfo()){
			scoreCard.add(new JLabel(s.split(" ")[0]));
			scoreCard.add(new JLabel(s.split(" ")[1]));
			scoreCard.add(new JLabel(s.split(" ")[2]));
		}
	}

	private void updateFrame(Game info){
		//will set the field u have all the pieces in the positions given by info
		//will set the side information to match that given by info
		ArrayList<Terrain> field = info.getField();
		int x=0;
		int y=0;
		for(JButton b : buttonsList){
			if(info.getTanks().get(playerNum).canSee(x,y))
				b.setIcon(icons.getTerrain(field.get(x+y*Bw)));
			else
				b.setIcon(icons.getFogTerrain(field.get(x+y*Bw)));
			x++;
			if(x==Bw){
				x=0;y++;
			}
		}
		int j=0;
		for(Tank t : info.getTanks()){
			if(t.isDead()){j++; continue;}
			if(j==playerNum){
				buttonsList.get(t.getX()+t.getY()*Bw).setIcon(icons.getMyTank(t.getDir()));				
			}else{
				if(info.getTanks().get(playerNum).canSee(t.getX(), t.getY()))buttonsList.get(t.getX()+t.getY()*Bw).setIcon(icons.getTank(t.getDir()));
			}
			j++;
		}
		gameLabels.get(0).setText(icons.playerName(playerNum)+"");
		gameLabels.get(3).setText("Turn: " + info.getTurn());
		gameLabels.get(4).setText(icons.playerName(info.getCurrentTank())+"");
		gameLabels.get(5).setText("Total HP: "+info.getTanks().get(playerNum).getHP());
		gameLabels.get(6).setText("Total MP: "+info.getTanks().get(playerNum).getMP());
		gameLabels.get(7).setText("Turn's MP: "+info.getTanks().get(playerNum).getMPthisTurn());
	}
	public void createOptionScreen(){
		JPanel secondScreen = new JPanel(new BorderLayout());
		JPanel Options = new JPanel(new GridLayout(6,2));
		secondScreen.add(Options);
		
		Options.add(new JLabel("BoardWidth:"));
		boardWidthText = new JTextField("10", 5);  
		Options.add(boardWidthText);
		
		Options.add(new JLabel("BoardHieght:"));
		boardHieghtText = new JTextField("10", 5);   
		Options.add(boardHieghtText);
		
		Options.add(new JLabel("Number Of Players:"));
		numOfPlayersText = new JTextField("2", 5);
		Options.add(numOfPlayersText);

		Options.add(new JLabel("Turns Per Player:"));
		turnsPerPlayerText = new JTextField("10", 5);
		Options.add(turnsPerPlayerText);

		Options.add(new JLabel("Time Per Turn:"));
		timePerTurnText = new JTextField("20", 5);
		Options.add(timePerTurnText);

		Options.add(new JLabel("Tank Health:"));
		tankHealthText = new JTextField("1", 5);
		Options.add(tankHealthText);

		CreateButton button = new CreateButton("Start the game!", client);
		button.addActionListener(this);
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(button);
		
		optionFrame.setVisible(false);
		optionFrame = new JFrame();
		optionFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		optionFrame.setLayout(new BorderLayout());
		optionFrame.setSize(new Dimension(500, 300));
		optionFrame.setTitle("Tanks");
		optionFrame.add(secondScreen);
		optionFrame.add(buttonPanel, BorderLayout.SOUTH);
		optionFrame.setVisible(true);
	}
	
	private JPanel createGameBoard(){
		//creates and returns a panel with an empty Go board on it, and fills up the buttonList 
		//array with all the BoardButtons created for the screen.
		JPanel retval = new JPanel(new GridLayout(Bh, Bw));
		int i = 0;
		buttonsList = new ArrayList<JButton>();
		while(i<Bw*Bh){
			BoardButton button = new BoardButton(i, client);
			button.setBackground(Color.BLACK);
			button.addActionListener(this);
			button.setBorderPainted(false);
			retval.add(button);
			buttonsList.add(button);
			i++;
		}
		return retval;
	}
	private JPanel createSideInfo(){
		//creates and returns a Panel with with info about the game and fills in the values of
		//turnLabels and scoreBoardText
		JPanel retval = new JPanel(new BorderLayout());
		
		//making player Name text
		JPanel playerName = new JPanel();
		JLabel name = new JLabel(icons.playerName(playerNum));
		name.setFont(new Font("Seriff", Font.BOLD, 20));
		playerName.add(name);
		gameLabels.add(name);
		
		//making option buttons
		JPanel buttons  = new JPanel(new FlowLayout());
		JPanel buttonsGrid = new JPanel(new GridLayout(10,1));
		buttons.add(buttonsGrid);

		SaveButton save = new SaveButton("Save Game", client);
		save.addActionListener(this);
		buttonsGrid.add(save);
		
		QuitButton quit = new QuitButton("End Game", client);
		quit.addActionListener(this);
		buttonsGrid.add(quit);
		
		PassButton pass = new PassButton("End Turn", client);
		pass.addActionListener(this);
		buttonsGrid.add(pass);

		ForwardButton forward = new ForwardButton("Move Forward - 2 MP", client);
		forward.addActionListener(this);
		buttonsGrid.add(forward);

		BackwardButton backward = new BackwardButton("Move Back - 3 MP", client);
		backward.addActionListener(this);
		buttonsGrid.add(backward);

		TurnButton left = new TurnButton("Turn Left - 1 MP", "left", client);
		left.addActionListener(this);
		buttonsGrid.add(left);

		TurnButton right = new TurnButton("Turn Right - 1 MP", "right", client);
		right.addActionListener(this);
		buttonsGrid.add(right);

		FireButton fire = new FireButton("Fire - 3 MP", client);
		fire.addActionListener(this);
		buttonsGrid.add(fire);

		ObserveButton obsv = new ObserveButton("Observe - 1 MP", client);
		obsv.addActionListener(this);
		buttonsGrid.add(obsv);

		ScanButton scan = new ScanButton("Scan - 5 MP", client);
		scan.addActionListener(this);
		buttonsGrid.add(scan);

		//making turn info
		JPanel turns = new JPanel(new GridLayout(4, 2));

		JLabel timeLeft = new JLabel("");
		gameLabels.add(timeLeft);
		turns.add(timeLeft);
		JLabel blank = new JLabel("");
		gameLabels.add(blank);
		turns.add(blank);		

		JLabel turnCounter = new JLabel("Error in createSideInfo");
		gameLabels.add(turnCounter);
		turns.add(turnCounter);		
		JLabel whosTurn = new JLabel("Error in createSideInfo");
		gameLabels.add(whosTurn);
		turns.add(whosTurn);
		JLabel HP = new JLabel("Error in createSideInfo");
		gameLabels.add(HP);
		turns.add(HP);
		JLabel MP = new JLabel("Error in createSideInfo");
		gameLabels.add(MP);
		turns.add(MP);
		JLabel currentMP = new JLabel("Error in createSideInfo");
		gameLabels.add(currentMP);
		turns.add(currentMP);
		
		//making score board
		retval.add(playerName, BorderLayout.NORTH);
		retval.add(buttons);
		JPanel scoreBoard = new JPanel(new BorderLayout());
		scoreBoard.add(turns, BorderLayout.NORTH);
		retval.add(scoreBoard,  BorderLayout.SOUTH);
		return retval;
	}
	
	private int parseHitSource(String s){
		s = s.substring(3);
		String source = "";
		while(s.charAt(0)!='o'){
			source+=s.charAt(0);
			s=s.substring(1);
		}
		return Integer.parseInt(source);
	}
	private int parseHitTarget(String s){
		s = s.substring(3);
		while(s.charAt(0)!='n') s=s.substring(1);
		s=s.substring(1);
		return Integer.parseInt(s);
	}
	public void soundEffect(String soundFileName) {
	      try {
	         File file = new File(soundFileName);
	    	 AudioInputStream stream = AudioSystem.getAudioInputStream(file);
	    	 AudioFormat format = stream.getFormat();
	    	// specify what kind of line we want to create
	    	 DataLine.Info info = new DataLine.Info(Clip.class, format);
	    	 // create the line
	    	 Clip clip = (Clip)AudioSystem.getLine(info);
	    	 // load the samples from the stream
	    	 clip.open(stream);
	    	 // begin playback of the sound clip
	    	 clip.start();
	      } catch (UnsupportedAudioFileException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      } catch (LineUnavailableException e) {
	         e.printStackTrace();
	      }
	   }
}