package TheGame;

import java.io.*;
import java.net.Socket;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class Player implements Runnable{
	private TankScreen screen;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private boolean notReady;
	
	//most of the methods in this class are called by buttons when they are told to act. This object will
	//respond by sending the request through the ObjectStream. This thread will wait for updates to come 
	//through the InputStream in the form of GameInfo and then tell the screen to update accordingly.
	//Methods that send information to the server should always send a String and that String should always
	//be >= 4 chars long
	public Player(TankScreen s){
		screen = s;
		notReady = true;
	}
	public void pass(){
		try{
			output.writeObject("pass");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void moveForwards(){
		try{
			output.writeObject("move");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void moveBackwards(){
		try{
			output.writeObject("revs");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void turn(String direction){
		try{
			output.writeObject("turn"+direction);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void fire(){
		try{
			output.writeObject("fire");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void scan(){
		try{
			output.writeObject("scan");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void directedScan(int loc){
		try{
			output.writeObject("dscn"+loc);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void observe(){
		try{
			output.writeObject("obsv");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void quit(){
		try{	
			output.writeObject("quit");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void save(){
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	try{
        		File file = fc.getSelectedFile();
        		PrintWriter writer = new PrintWriter(file, "UTF-8");
        		writer.println(screen.mostRecentInfo.toString());
        		writer.close();
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
	}
	public void join(String s){
		//join a game, this will join a game at the IP number give in s, and port# 8189
		boolean validIP = true;
		while(screen.getName().equals("")) screen.setName(JOptionPane.showInputDialog(null, "Username: "));
		try{
			Socket server = new Socket(s, 8189);
			output = new ObjectOutputStream(server.getOutputStream());
			input = new ObjectInputStream(server.getInputStream());
		}catch(Exception e){
			validIP = false;
		}
		if(validIP){
			try {
				screen.Bw = input.readInt();
				screen.Bh = input.readInt();
				screen.numOfPlayers = input.readInt();
				screen.timePerTurn = input.readInt();
				screen.playerNum = input.readInt();
				input.readInt(); //Stall until all players join
				screen.becomeBoard();
				output.writeObject("name"+screen.getName());
				notReady = false;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			String str = JOptionPane.showInputDialog(null, "Invalid IP, new IP:");
			if(str.equals(""))str = "localhost";
			join(str);
		}
	}
	public void load(){
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	try{
        		File file = fc.getSelectedFile();
        		BufferedReader br = new BufferedReader(new FileReader(file));
        		String savedGame = br.readLine();
        		NetworkController server = new NetworkController(savedGame);
        		Thread t = new Thread(server);
        		t.start();
        		
        		join("localhost");
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }

	}
	public void create(){
		NetworkController server = new NetworkController(screen.Bw, screen.Bh, screen.numOfPlayers, screen.turnsPerPlayer, screen.timePerTurn, screen.tankHealth);
		Thread t = new Thread(server);
		t.start();
		
		join("localhost");
	}
	public void next(){
		//This changes the frame in GameScreen from being the intro screen to being the option screen to create
		//a server/game
		screen.createOptionScreen();
	}
	public void run(){
		try{
			while(notReady){Thread.sleep(10);}
			while(true){
				Game info = (Game) input.readObject();
				if(info!=null) screen.setNames(info.getNames());
				screen.update(info);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

