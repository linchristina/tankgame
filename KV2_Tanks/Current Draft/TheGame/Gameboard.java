package TheGame;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.*;

public class Gameboard{
	// This object will keep track of the board and do all necessary calculations for the board. 
	// This object has 7 public methods:
	//      -moveForwards()
	//      -moveBackwards()
	//      -pass()
    //      -turn( string direction)
	//      -fire()
	//      -scan()
	//      -observer()
	//		-quit()
	private int numOfPlayers;    //should always be >1
	private int Bw;
	private int Bh;
	public int totalTurns;
	private Game currentInfo;

	
	public Gameboard(int boardWidth, int boardHieght, int numberOfPlayers, int turnsPerPlayer, int tankHealth){
		//This is initialize all variables and create a board that is empty
		Bw = boardWidth;
		Bh = boardHieght;
		numOfPlayers = numberOfPlayers;
		totalTurns = turnsPerPlayer*numOfPlayers;
		currentInfo = new Game(Bw, Bh, numOfPlayers);
		currentInfo.setField(makeEmptyField());
		currentInfo.setTurn(0);
		for(int i=0;i<numOfPlayers;i++){
			//Choose tank x and y, repick if a tank is already there, or its ontop of an obstacle
			int x, y;
			do{
				x = (int)Math.floor(Math.random()*Bw);
				y = (int)Math.floor(Math.random()*Bh);
			}while(currentInfo.hasTank(x,y) || currentInfo.getField().get(x+y*Bw) instanceof Obstacle);

			int dir = (int)Math.floor(Math.random()*4);
			currentInfo.addTank(
					new Tank(x,y,dir,tankHealth,100,i,currentInfo));
		}
	}
	public Gameboard(Game g,int boardWidth, int boardHieght, int numberOfPlayers, int turnsPerPlayer, int tankHealth){
		//This is initialize all variables and create a board that is empty
		Bw = boardWidth;
		Bh = boardHieght;
		numOfPlayers = numberOfPlayers;
		totalTurns = turnsPerPlayer*numOfPlayers;
		currentInfo=g;
	}
	private ArrayList<Terrain> makeEmptyField(){
		//will create an array Bw*Bh long, and fill it with terrain
		ArrayList<Terrain> retval;
		double normal   = 0.5;
		double pit      = 0.7;
		double hill     = 0.9;
		double obstacle = 1.0;
		do{
			retval = new ArrayList<Terrain>();
			for(int y=0;y<Bh;y++){
				for(int x=0;x<Bw;x++){				
					double rand = Math.random();
					if(rand<normal) retval.add(new Terrain(x,y));
					if(rand>=normal && rand<pit) retval.add(new Pit(x,y));
					if(rand>=pit && rand<hill) retval.add(new Hill(x,y));
					if(rand>=hill) retval.add(new Obstacle(x,y));
				}
			}
			//reduce probabilities of obstacles, ensures we stop building maps after 2 iterations, worst case
			hill+=0.05;
		}while(!validMap(retval));
		
		return retval;
	}
	public Game moveForwards(){
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canMoveForwards()){
			currentInfo.setSpecialCase(null);
			currentTank.moveForwards();
			if(currentTank.getMPthisTurn()==0) pass();
			return currentInfo.copyOf();
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	public Game moveBackwards(){
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canMoveBackwards()){
			currentInfo.setSpecialCase(null);
			currentTank.moveBackwards();
			if(currentTank.getMPthisTurn()==0) pass();
			return currentInfo.copyOf();
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	public Game turn(String direction){
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canTurn()){
			currentInfo.setSpecialCase(null);
			currentTank.turn(direction);
			if(currentTank.getMPthisTurn()==0) pass();
			return currentInfo.copyOf();
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	public Game fire(){
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canFire()){
			currentInfo.setSpecialCase(null);
			currentTank.fire();
			if(currentTank.getMPthisTurn()==0) pass();
			Game ret = currentInfo.copyOf();
			currentInfo.setSpecialCase(null);
			return ret;
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	public Game scan(){
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canScan()){
			currentInfo.setSpecialCase(null);
			currentTank.scan();
			if(currentTank.getMPthisTurn()==0) pass();
			return currentInfo.copyOf();
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	public Game directedScan(int loc){
		int x = loc%Bw;
		int y = loc/Bw;
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canDirectedScan() && x<Bw && x>=0 && y<Bh && y>=0){
			currentInfo.setSpecialCase(null);
			currentTank.directedScan(loc);
			if(currentTank.getMPthisTurn()==0) pass();
			return currentInfo.copyOf();
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	public Game observe(){
		Tank currentTank = currentInfo.getTanks().get(currentInfo.getCurrentTank());
		if(currentTank.canObserve()){
			currentInfo.setSpecialCase(null);
			currentTank.observe();
			if(currentTank.getMPthisTurn()==0) pass();
			return currentInfo.copyOf();
		}else{
			Game specialCase = currentInfo.copyOf();
			specialCase.setSpecialCase("Invalid Move");
			return specialCase;
		}
	}
	
	public Game pass(){
		//This is called by the Server and will cause move the turn forward one,
		//and return the new currentInfo
		currentInfo.nextTurn();
		currentInfo.getTanks().get(currentInfo.getCurrentTank()).startTurn(5);
		return currentInfo.copyOf();
	}
	
	public Game getCurrentInfo(){
		return currentInfo;
	}
	public int getTurn(){
		return currentInfo.getTurn();
	}
	public int getCurrentTank(){
		return currentInfo.getCurrentTank();
	}
	public Game quit(int playerNum){
		currentInfo.getTanks().get(playerNum).setHP(0);
		if(currentInfo.getCurrentTank()==playerNum)currentInfo.nextTurn();
		return currentInfo.copyOf();
	}
	public Game endGame() throws SQLException{
		Game retval = currentInfo.copyOf();
		
		//update database
		UpdateInfo db = null;
		try {
			db = new UpdateInfo();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int win = currentInfo.winner();
		int current=0;
		for(String s : currentInfo.getNames()){
			String record = db.getUserInfo(s);
			String winTotal, lossTotal;

			if(record==null){
				db.insertUserInfo(s,0,0);
				winTotal = "0";
				lossTotal = "0";
			}else{
				winTotal = record.split(" ")[1];
				lossTotal = record.split(" ")[2];
			}
			if(win==current){
				db.updateUserInfo(s, Integer.parseInt(winTotal)+1, Integer.parseInt(lossTotal));
			}else{
				db.updateUserInfo(s, Integer.parseInt(winTotal), Integer.parseInt(lossTotal)+1);				
			}
			current++;
		}
		retval.setStatistics(db);
		db.close();

		retval.setSpecialCase("Game Over");
		return retval;
	}
	private boolean validMap(ArrayList<Terrain> t){
		boolean [][] mark = new boolean[Bw][Bh];
		int s = 0;
		while(t.get(s) instanceof Obstacle)s++;

		mark[s%Bw][s/Bw]=true;
		recursValidMap(s, mark, t);
		
		for(int x=0;x<Bw;x++){
			for(int y=0;y<Bh;y++){
				if(!mark[x][y] && !(t.get(x+Bw*y) instanceof Obstacle))return false;
			}
		}
		return true;
	}
	private void recursValidMap(int s, boolean [][] mark, ArrayList<Terrain> t){
		int x = s%Bw;
		int y = s/Bw;
		if(x!=0 && !mark[x-1][y] && !(t.get(x-1+Bw*y) instanceof Obstacle)){
			mark[x-1][y]=true;
			recursValidMap(x-1+y*Bw, mark, t);
		}
		if(y!=0 && !mark[x][y-1] && !(t.get(x+Bw*(y-1)) instanceof Obstacle)){
			mark[x][y-1]=true;
			recursValidMap(x+(y-1)*Bw, mark, t);
		}
		if(x!=Bw-1 && !mark[x+1][y] && !(t.get(x+1+Bw*y) instanceof Obstacle)){
			mark[x+1][y]=true;
			recursValidMap(x+1+y*Bw, mark, t);
		}
		if(y!=Bh-1 && !mark[x][y+1] && !(t.get(x+Bw*(y+1)) instanceof Obstacle)){
			mark[x][y+1]=true;
			recursValidMap(x+(y+1)*Bw, mark, t);
		}
	}
}
