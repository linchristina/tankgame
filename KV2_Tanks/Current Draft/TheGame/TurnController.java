package TheGame;

import java.io.Serializable;

public class TurnController implements Serializable{
	private int turn;
	private int currentTank;
	private Game info;
	private int numPlayers;
	public TurnController(Game g, int np){
		turn=0;
		currentTank=0;
		info=g;
		numPlayers=np;
	}
	public void next(){ 
		turn++;
		currentTank = (currentTank+1)%numPlayers;
		while(info.getTanks().get(currentTank).isDead()){
			currentTank = (currentTank+1)%numPlayers;
		}
	}
	public void setTurn(int t){
		turn=t;
	}
	public int getTurn(){
		return turn;
	}
	public void setCurrentTank(int ct){
		currentTank=ct;
	}
	public int getCurrentTank(){
		return currentTank;
	}
}
