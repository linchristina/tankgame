package TheGame;

public class LoginController {
	private String username;
	
	public LoginController(){
		username = null;
	}
	
	public boolean register(String name){
		username=name;
		return true; //Name is valid
	}
	public boolean uniqueName(String []names){
		for(String s : names){
			if(s.equals(username))return false;
		}
		return true;
	}
	public String getUsername(){
		return username;
	}
}
