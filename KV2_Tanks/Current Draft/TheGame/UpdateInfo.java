/*account:kv2 
 *password:cs487
 *database name: project_kv2
 *table name: PLAYERINFO
 *int String int int
 *id, NAME, WIN, LOSS*/

package TheGame;

import java.sql.*;

public class UpdateInfo {
	
	private Connection connect = null;
	private Statement statement = null;
	
	private boolean SKIP_DATABASE=true;
	//establish link
	
	public UpdateInfo() throws Exception {
		if(SKIP_DATABASE)return;
		try {
			//load MySQL driver
			Class.forName("com.mysql.jdbc.Driver");
			//setup connection
			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/project_kv2?"
					+"user=kv2&password=cs487");
			//create statement
			statement = connect.createStatement();
		} catch (Exception e) {
			throw e;
		}
	}
	
	/*this method receive a user name and return the info of that user in a String "name win loss" form
	 *this method will check if input user exist, if not, return null*/
	public String getUserInfo(String name) throws SQLException {
		if(SKIP_DATABASE)return "Bob 17 18";
		String query = "select * from PROJECT_KV2.PLAYERINFO where name = '"+name+"'";
		String userinfo = null;
		ResultSet info = statement.executeQuery(query);
		if(info.next())
			userinfo = info.getString("name")+" "+info.getInt("win")+" "+info.getInt("loss");
		info.close();
		return userinfo;
	}
	
	//this method receive a String name, a int win, a int loss of new user info and insert it into data base 
	public void insertUserInfo(String name, int win, int loss) throws SQLException {
		if(SKIP_DATABASE)return;

		String query = "insert into PROJECT_KV2.PLAYERINFO values (default,?,?,?)";
		PreparedStatement Pstatement = connect.prepareStatement(query);
		Pstatement.setString(1,name);
		Pstatement.setInt(2,win);
		Pstatement.setInt(3,loss);
		Pstatement.executeUpdate();
		Pstatement.close();
	}
	
	//this method receive a String name, a int win, a int loss of update user info and update it in data base
	public void updateUserInfo(String name, int win, int loss) throws SQLException {
		if(SKIP_DATABASE)return;

		String query = "update PROJECT_KV2.PLAYERINFO set WIN = ?, LOSS = ? WHERE NAME = ?";
		PreparedStatement Pstatement = connect.prepareStatement(query);
		Pstatement.setString(3,name);
		Pstatement.setInt(1,win);
		Pstatement.setInt(2,loss);
		Pstatement.executeUpdate();
		Pstatement.close();
	}
	
	//this method delete a user info based on name given
	public void deleteUserInfo(String name) throws SQLException {
		if(SKIP_DATABASE)return;

		String query = "delete from PROJECT_KV2.PLAYERINFO where name = '"+name+"'";
		Statement deletestatement = connect.createStatement();
		deletestatement.executeUpdate(query);
		deletestatement.close();
	}
	
	//this method delete a user info based on primary id
	public void deleteUserInfo(int id) throws SQLException {
		if(SKIP_DATABASE)return;

		String query = "delete from PROJECT_KV2.PLAYERINFO where id = "+id+"";
		Statement deletestatement = connect.createStatement();
		deletestatement.executeUpdate(query);
		deletestatement.close();
	}
	
	//this method will show all content in the table
	public void showAll() throws SQLException {
		if(SKIP_DATABASE)return;

		String query = "select * from PROJECT_KV2.PLAYERINFO";
		Statement showstatement = connect.createStatement();
		ResultSet info = showstatement.executeQuery(query);
		while (info.next())
			System.out.println(info.getInt("id")+" "+info.getString("name")+" "+info.getInt("win")+" "+info.getInt("loss"));
		info.close();
		showstatement.close();
	}
	
	//close everything
	public void close() throws SQLException {
		if(SKIP_DATABASE)return;

		statement.close();
		connect.close();
	}
}
