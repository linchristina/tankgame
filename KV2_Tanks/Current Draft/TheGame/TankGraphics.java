package TheGame;

import java.awt.*; 
import java.awt.image.*;
import java.util.ArrayList;

import javax.swing.ImageIcon;


public class TankGraphics {
	//extending classes should implement playerIcon, playerIconFocus, and playerIconInfl 
	
	private ImageIcon background;	
	private ImageIcon normal_terrain;
	private ImageIcon hill_terrain;
	private ImageIcon pit_terrain;
	private ImageIcon obstacle_terrain;
	private ImageIcon normal_fog_terrain;
	private ImageIcon hill_fog_terrain;
	private ImageIcon pit_fog_terrain;
	private ImageIcon obstacle_fog_terrain;
	private ImageIcon myTank0;
	private ImageIcon myTank1;
	private ImageIcon myTank2;
	private ImageIcon myTank3;
	private ImageIcon tank0;
	private ImageIcon tank1;
	private ImageIcon tank2;
	private ImageIcon tank3;
	
	private ArrayList<String> names;
	
	public TankGraphics(){
		background = new ImageIcon(getClass().getResource("/kv-2-background.jpg"));
		normal_terrain = new ImageIcon(getClass().getResource("/normal.jpg"));
		hill_terrain = new ImageIcon(getClass().getResource("/hill.jpg"));
		pit_terrain = new ImageIcon(getClass().getResource("/pit.jpg"));
		obstacle_terrain = new ImageIcon(getClass().getResource("/obstacle.jpg"));
		normal_fog_terrain = new ImageIcon(getClass().getResource("/fog-normal.jpg"));
		hill_fog_terrain = new ImageIcon(getClass().getResource("/fog-hill.jpg"));
		pit_fog_terrain = new ImageIcon(getClass().getResource("/fog-pit.jpg"));
		obstacle_fog_terrain = new ImageIcon(getClass().getResource("/fog-obstacle.jpg"));
		myTank0 = new ImageIcon(getClass().getResource("/myTank0.jpg"));
		myTank1 = new ImageIcon(getClass().getResource("/myTank1.jpg"));
		myTank2 = new ImageIcon(getClass().getResource("/myTank2.jpg"));
		myTank3 = new ImageIcon(getClass().getResource("/myTank3.jpg"));
		tank0 = new ImageIcon(getClass().getResource("/tank0.jpg"));
		tank1 = new ImageIcon(getClass().getResource("/tank1.jpg"));
		tank2 = new ImageIcon(getClass().getResource("/tank2.jpg"));
		tank3 = new ImageIcon(getClass().getResource("/tank3.jpg"));
	}
	
	public ImageIcon getBackgroundPic(){
		return background;
	}	
	
	public ImageIcon getTerrain(Terrain t){
		if(t instanceof Hill){
			return hill_terrain;
		}else if(t instanceof Pit){
			return pit_terrain;
		}else if(t instanceof Obstacle){
			return obstacle_terrain;
		}else{
			return normal_terrain;
		}
	}
	public ImageIcon getFogTerrain(Terrain t){
		if(t instanceof Hill){
			return hill_fog_terrain;
		}else if(t instanceof Pit){
			return pit_fog_terrain;
		}else if(t instanceof Obstacle){
			return obstacle_fog_terrain;
		}else{
			return normal_fog_terrain;
		}
	}
	public ImageIcon getMyTank(int dir){
		if(dir==0) return myTank0;
		if(dir==1) return myTank1;
		if(dir==2) return myTank2;
		if(dir==3) return myTank3;
		return null;
	}
	public ImageIcon getTank(int dir){
		if(dir==0) return tank0;
		if(dir==1) return tank1;
		if(dir==2) return tank2;
		if(dir==3) return tank3;
		return null;
	}

	public String playerName(int playerNum){
		if(names!=null && names.size()>playerNum)return names.get(playerNum);
		return ""+(playerNum+1);
	}
	
	public void setNames(ArrayList<String> names){
		this.names=names;
	}
}