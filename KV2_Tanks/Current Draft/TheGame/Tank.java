package TheGame;

import java.io.Serializable;
import java.util.LinkedList;

public class Tank implements Serializable{

	private int x;
	private int y;
	private int dir;
	private int HP;
	private int MP;
	private int MPthisTurn =5;
	private int id;
	private boolean observing=false;
	private boolean scanning=false;
	private int directedScanPoint;
	
	private int moveForwardsCost = 2;
	private int moveBackwardsCost = 3;
	private int turnCost = 1;
	private int fireCost = 3;
	private int scanCost = 5;
	private int directedScanCost = 3;
	private int observeCost = 1;

	private Game info;
	
	public Tank(int initX, int initY, int initDir, int HP, int MP, int id, Game i){
		x=initX;
		y=initY;
		dir=initDir;
		this.HP=HP;
		this.MP=MP;
		this.id=id;
		info = i;
		directedScanPoint = -1;
	}
	public int getX(){ return x; }
	public int getY(){ return y; }
	public int getDir(){ return dir; }
	public int getHP(){ return HP; }
	public void setHP(int h){ HP = h; }
	public int getMP(){ return MP; }
	public int getId(){ return id; }
	
	public void setObserving(boolean b){ observing = b; }
	public boolean getObserving(){ return observing; }

	public void setScanning(boolean b){ scanning = b; }
	public boolean getScanning(){ return scanning; }

	public int getMPthisTurn(){ return MPthisTurn; }
	public void setMPthisTurn(int m){ MPthisTurn = m;}

	public void setDirectedScanPoint(int l){ directedScanPoint = l; }
	public int getDirectedScanPoint(){ return directedScanPoint; }

	public void startTurn(int MPthisTurn){
		this.MPthisTurn = Math.min(MPthisTurn, MP);
		this.observing=false;
		this.scanning=false;
		directedScanPoint = -1;
	}
	public void hit(){
		HP--;
	}
	public boolean canMoveForwards() {
		if(MPthisTurn<moveForwardsCost)return false;
		if(dir==0 && !valid(x, y-1)) return false;
		if(dir==1 && !valid(x+1, y)) return false;
		if(dir==2 && !valid(x, y+1)) return false;
		if(dir==3 && !valid(x-1, y)) return false;
		return true;
	}
	public void moveForwards() {
		MPthisTurn-=moveForwardsCost;
		MP-=moveForwardsCost;
		if(dir==0) y--;
		if(dir==1) x++;
		if(dir==2) y++;
		if(dir==3) x--;
	}
	public boolean canMoveBackwards() {
		if(MPthisTurn<moveBackwardsCost)return false;
		if(dir==0 && !valid(x, y+1)) return false;
		if(dir==1 && !valid(x-1, y)) return false;
		if(dir==2 && !valid(x, y-1)) return false;
		if(dir==3 && !valid(x+1, y)) return false;
		return true;
	}
	public void moveBackwards() {
		MPthisTurn-=moveBackwardsCost;
		MP-=moveBackwardsCost;
		if(dir==0) y++;
		if(dir==1) x--;
		if(dir==2) y--;
		if(dir==3) x++;
	}
	public boolean canTurn() {
		if(MPthisTurn<turnCost)return false;
		return true;
	}
	public void turn(String direction) {
		MPthisTurn-=turnCost;
		MP-=turnCost;
		if(direction.equals("right")) dir = (dir+1)%4;
		else dir = (dir+3)%4;
	}
	public boolean canFire() {
		if(MPthisTurn<fireCost)return false;
		return true;
	}
	public void fire() {
		MPthisTurn-=fireCost;
		MP-=fireCost;
		int xx, yy;
		if(dir==0){
			for(yy = y-1; valid(x,yy); yy--);
			if(info.hasTank(x, yy)){
				info.setSpecialCase("hit"+id+"on"+info.getTank(x,yy).getId());
				info.getTank(x,yy).hit();
			}
		}
		if(dir==1){
			for(xx = x+1; valid(xx,y); xx++);
			if(info.hasTank(xx, y)){
				info.setSpecialCase("hit"+id+"on"+info.getTank(xx,y).getId());
				info.getTank(xx,y).hit();
			}
		}
		if(dir==2){
			for(yy = y+1; valid(x,yy); yy++);
			if(info.hasTank(x, yy)){
				info.setSpecialCase("hit"+id+"on"+info.getTank(x,yy).getId());
				info.getTank(x,yy).hit();
			}
		}
		if(dir==3){
			for(xx = x-1; valid(xx,y); xx--);
			if(info.hasTank(xx, y)){
				info.setSpecialCase("hit"+id+"on"+info.getTank(xx,y).getId());
				info.getTank(xx,y).hit();
			}
		}
	}
	public boolean canScan() {
		if(MPthisTurn<scanCost)return false;
		return true;
	}
	public void scan() {
		MPthisTurn-=scanCost;
		MP-=scanCost;
		scanning=true;
	}
	public boolean canDirectedScan() {
		if(MPthisTurn<directedScanCost)return false;
		return true;
	}
	public void directedScan(int loc) {
		MPthisTurn-=directedScanCost;
		MP-=directedScanCost;
		directedScanPoint=loc;
	}
	public boolean canObserve() {
		if(MPthisTurn<observeCost)return false;
		return true;
	}
	public void observe() {
		MPthisTurn-=observeCost;
		MP-=observeCost;
		observing=true;
	}
	private boolean valid(int x, int y){
		if(x>=info.getBw() || x<0 || y>=info.getBh() || y<0 
				|| info.getField().get(x+y*info.getBw()) instanceof Obstacle
				|| info.hasTank(x,y)) return false;
		return true;
	}
	public boolean canSee(int x, int y){
		if(scanning && info.hasTank(x, y))return true;
		System.out.println(directedScanPoint);
		if(directedScanPoint!=-1){
			int px = directedScanPoint%info.getBw();
			int py = directedScanPoint/info.getBw();
			int dx = Math.abs(x-px);
			int dy = Math.abs(y-py);
			if(dx+dy <= 2) return true; //This scan point can see the locations
		}
		
		int vision = 3;
		if(observing)vision++;
		if(info.getField().get(this.x+this.y*info.getBw()) instanceof Hill)vision++;
		if(info.getField().get(this.x+this.y*info.getBw()) instanceof Pit)vision--;
		int dx = Math.abs(x-this.x);
		int dy = Math.abs(y-this.y);
		
		return dx+dy<=vision;
	}
	public boolean isDead(){
		return HP==0 || MP==0;
	}
}
