package TheGame;

import java.io.IOException; 
import java.net.*;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.locks.*;

public class NetworkController implements Runnable{
	//The Server is a thread so that the user is able to create a server and still act as a client
	//This thread will create a TankClientThread thread for each client who joins the game. These threads
	//will then inform the server of any request made by that client
	
	private ArrayList<TankClientThread> clients;
	private Gameboard theBoard;
	private int numOfPlayers;
	private int Bw;
	private int Bh;
	private int turnsPerPlayer;
	private int timePerTurn;
	private int tankHealth;
	private Lock serverLock = new ReentrantLock();
	
	private boolean saved;
	
	public NetworkController(int Bw, int Bh, int numOfPlayers, int turnsPerPlayer, int timePerTurn, int tankHealth){
		saved=false;
		theBoard = new Gameboard(Bw, Bh, numOfPlayers, turnsPerPlayer, tankHealth);
		this.Bw = Bw;
		this.Bh = Bh;
		this.numOfPlayers = numOfPlayers;
		this.turnsPerPlayer = turnsPerPlayer;
		this.timePerTurn = timePerTurn;
		this.tankHealth = tankHealth;
		clients = new ArrayList<TankClientThread>();
	}
	public NetworkController(String savedGame){
		saved=true;
		Game g=Game.fromString(savedGame);
		this.Bw = g.getBw();
		this.Bh = g.getBh();
		this.numOfPlayers = g.getNumPlayers();
		this.turnsPerPlayer = 10;
		this.timePerTurn = 20;
		this.tankHealth=1;
		theBoard = new Gameboard(g,Bw, Bh, numOfPlayers, turnsPerPlayer, tankHealth);
		clients = new ArrayList<TankClientThread>();
	}
	public void run(){
		try{
			int playersJoined = 0;
			ServerSocket s = new ServerSocket(8189); //should always be an available port
			while(playersJoined<numOfPlayers){
				Socket incoming = s.accept();
				TankClientThread client = new TankClientThread(playersJoined, this, incoming );
				clients.add(client);
				client.returnGameInfo(Bw, Bh, numOfPlayers, timePerTurn);
				
				Thread t = new Thread(client);
				t.start();
				System.out.println("Just got player "+playersJoined+" out of "+numOfPlayers);
				playersJoined++;
			 }
			for(TankClientThread t : clients){
				t.sendSignal();
				t.returnField(theBoard.getCurrentInfo());
			}
			s.close();
		}catch(IOException e){ 
			e.printStackTrace();
		}
	}
	
	public void request(int playerNum, String s){
		serverLock.lock();
		try{
			Game info = theBoard.getCurrentInfo().copyOf();
			if(info.getTurn()==theBoard.totalTurns){
				try {
					info = theBoard.endGame();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				returnAll(info);
				return;
			}
			if(s.equals("quit")){
				info = theBoard.quit(playerNum);
				if(info.countLivingTanks()==1){
					try {
						info = theBoard.endGame();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					returnAll(info);
					return;
				}else{
					returnAll(info);
				}
			}
			if(s.substring(0,4).equals("name")){
				if(!saved) theBoard.getCurrentInfo().addName(s.substring(4));
				returnAll(theBoard.getCurrentInfo().copyOf());
				return;
			}
			System.out.println(theBoard.getCurrentInfo().getNames().size());
			System.out.println(numOfPlayers);
			if(theBoard.getCurrentInfo().getNames().size()!=numOfPlayers){
				info = theBoard.getCurrentInfo().copyOf();
				info.setSpecialCase("Wait");
				returnAll(info);
				info.setSpecialCase(null);
				return;
			}
			if(playerNum == theBoard.getCurrentTank()){  
				if(s.equals("pass")){
					info = theBoard.pass();
					returnAll(info);
				}
				if(s.substring(0,4).equals("move")){ //move forwards
					info = theBoard.moveForwards();
					if(info.hasSpecialCase()){
						returnTo(playerNum, info);
					}else{
						returnAllExcept(playerNum, info);
						info.setSound("move");
						returnTo(playerNum, info);
					}
				}
				if(s.substring(0,4).equals("revs")){ //move backwards
					info = theBoard.moveBackwards();
					if(info.hasSpecialCase()){
						returnTo(playerNum, info);
					}else{
						returnAllExcept(playerNum, info);
						info.setSound("move");
						returnTo(playerNum, info);
					}
				}
				if(s.substring(0,4).equals("turn")){
					String direction = s.substring(4);
					info = theBoard.turn(direction);
					if(info.hasSpecialCase()){
						returnTo(playerNum, info);
					}else{
						returnAllExcept(playerNum, info);
						info.setSound("move");
						returnTo(playerNum, info);
					}
				}
				if(s.substring(0,4).equals("fire")){
					info = theBoard.fire();
					if(info.hasSpecialCase() && !info.getSpecialCase().substring(0,3).equals("hit")){
						returnTo(playerNum, info);
					}else{
						returnAllExcept(playerNum, info);
						info.setSound("fire");
						returnTo(playerNum, info);
					}
				}
				if(s.substring(0,4).equals("scan")){
					info = theBoard.scan();
					if(info.hasSpecialCase()){
						returnTo(playerNum, info);
					}else{
						returnAll(info);
					}
				}
				if(s.substring(0,4).equals("dscn")){ //directed scan
					info = theBoard.directedScan(Integer.parseInt(s.substring(4)));
					if(info.hasSpecialCase()){
						returnTo(playerNum, info);
					}else{
						returnAll(info);
					}
				}
				if(s.substring(0,4).equals("obsv")){
					info = theBoard.observe();
					if(info.hasSpecialCase()){
						returnTo(playerNum, info);
					}else{
						returnAll(info);
					}
				}
			}else{
				info.setSpecialCase("Not Your Turn");
				returnTo(playerNum, info);
			}
			if(info.countLivingTanks()==1){
				try {
					info = theBoard.endGame();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				returnAll(info);
			}
		}finally{
			serverLock.unlock();
		}
	}
	private void returnAll(Game info){
		for(TankClientThread client : clients){
			client.returnField(info);
		}
	}
	private void returnAllExcept(int playerNum, Game info){
		int i=0;
		for(TankClientThread client : clients){
			if(i++==playerNum)continue;
			client.returnField(info);
		}
	}
	private void returnTo(int playerNum, Game info){
		clients.get(playerNum).returnField(info); 
	}
}