package TheGame;

import java.io.*;
import java.net.*;

public class TankClientThread implements Runnable{
	
	private int playerNum;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private NetworkController theServer;
	private boolean gameOver;
	
	public TankClientThread(int playerNum, NetworkController theServer, Socket incoming){
		this.playerNum = playerNum;
		this.theServer = theServer;
		gameOver=false;
		try{
			input = new ObjectInputStream(incoming.getInputStream());
			output = new ObjectOutputStream(incoming.getOutputStream());
			
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void run(){	
		//This will wait to receive a request from the client and then pass it along to the server
		try{
			while(!gameOver){
				String command = (String) input.readObject();
				theServer.request(playerNum, command);
			}
			input.close();
			output.close();
		}catch(Exception e){ 
			e.printStackTrace();
		}
	}
	
	public void returnField(Game info){
		try{
			output.writeObject(info);
			if(info.hasSpecialCase() && info.getSpecialCase().equals("GameOver"))gameOver = true;
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void sendSignal(){
		try{
			output.writeInt(0);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public void returnGameInfo(int Bw, int Bh, int numOfPlayers, int timePerTurn){
		try{
			output.writeInt(Bw);
			output.writeInt(Bh);
			output.writeInt(numOfPlayers);
			output.writeInt(timePerTurn);
			output.writeInt(playerNum);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
