package Buttons;

import javax.swing.JButton;

import TheGame.Player;

public class TankButton extends JButton {
	
	public final Player client;
	
	public TankButton(Player c){
		super();
		client = c;
	}
	public TankButton(String s, Player c){
		super(s);
		client = c;
	}
	public TankButton(String s){
		super(s);
		client = null;
	}
	public void act(){
		
	}
}
