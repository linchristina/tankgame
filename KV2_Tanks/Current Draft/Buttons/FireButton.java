package Buttons;

import TheGame.Player;


public class FireButton extends TankButton{
	//This button will tell the GoBoard to pass then return the GoInfo for the 
	//new board back to the GoScreen
	
	public FireButton(String s, Player c){
		super(s, c);
	}
	public void act(){
		client.fire();
	}
}
