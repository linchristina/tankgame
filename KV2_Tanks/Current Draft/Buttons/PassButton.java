package Buttons;

import TheGame.Player;


public class PassButton extends TankButton{
	//This button will tell the GoBoard to pass then return the GoInfo for the 
	//new board back to the GoScreen
	
	public PassButton(String s, Player c){
		super(s, c);
	}
	public void act(){
		client.pass();
	}
}
