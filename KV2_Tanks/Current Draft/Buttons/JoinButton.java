package Buttons;

import javax.swing.JOptionPane;

import TheGame.Player;

public class JoinButton extends TankButton{
	public JoinButton(String s, Player c){
		super(s,c);
	}
	public void act(){
		String s = JOptionPane.showInputDialog(null, "Host IP: ");
		if(s.equals("")) s = "localhost";
		client.join(s);
	}
}
