package Buttons;

import TheGame.Player;

public class QuitButton extends TankButton {
	
	public QuitButton(String s, Player c){
		super(s, c);
	}
	
	public void act(){
		client.quit();
	}
}
