package Buttons;

import TheGame.Player;


public class ForwardButton extends TankButton{
	//This button will tell the GoBoard to pass then return the GoInfo for the 
	//new board back to the GoScreen
	
	public ForwardButton(String s, Player c){
		super(s, c);
	}
	public void act(){
		client.moveForwards();
	}
}
