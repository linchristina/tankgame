package Buttons;

import TheGame.Player;


public class TurnButton extends TankButton{
	//This button will tell the GoBoard to pass then return the GoInfo for the 
	//new board back to the GoScreen
	private String dir;
	public TurnButton(String s, String direction, Player c){
		super(s, c);
		dir = direction;
	}
	public void act(){
		client.turn(dir);
	}
}
