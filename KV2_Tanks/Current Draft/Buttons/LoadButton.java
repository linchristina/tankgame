package Buttons;

import TheGame.Player;

public class LoadButton extends TankButton{
	public LoadButton(String s, Player c){
		super(s,c);
	}
	public void act(){ 
		client.load();  //number of computers
	}
}
