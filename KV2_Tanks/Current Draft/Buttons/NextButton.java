package Buttons;

import TheGame.Player;

public class NextButton extends TankButton {

	public NextButton(String s, Player c) {
		super(s, c);
	}
	public void act(){
		client.next();
	}

}
