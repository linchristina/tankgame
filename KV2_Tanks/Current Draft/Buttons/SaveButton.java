package Buttons;

import TheGame.Player;

public class SaveButton extends TankButton {
	
	public SaveButton(String s, Player c){
		super(s, c);
	}
	
	public void act(){
		client.save();
	}
}
