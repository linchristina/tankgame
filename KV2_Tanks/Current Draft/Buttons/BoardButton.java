package Buttons;

import TheGame.Player;


public class BoardButton extends TankButton{
	//This button will tell the GoBoard to make a move then return the GoInfo for the
	//new board back to the GoScreen
	private int myPosition;
	
	public BoardButton(int position, Player c){
		super(c);
		myPosition = position;
	}
	
	public void act(){
		client.directedScan(myPosition);
	}

}
